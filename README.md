# primo-to-ares-push-to

In classic Primo UI an action  request was made available to parse details from the primo ui and then send those into an Ares Item request form using URL parms.  Ares would handle the authentication / permissions portion.

When the tabs on the primo UI are clicked, generate an *action* item for the list...basically build a link into the appropriate Ares system, pushing some
record data

At the time of implementation, there wasn't an API to use to get the info, so instead the UI was parsed. In hindsight, using the showpnx url parm and making 
an additional request would have been a much better solution more than likely.

Files:
1. v3_TUG_primo_ares_push_to.js: This one is included in Primo, the discovery layer
2. ares_push_to_include.js: This must be included on the Ares html pages, to work around an Ares permissions issue