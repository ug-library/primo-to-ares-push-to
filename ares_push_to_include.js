function tugCheckForCourses() {
	//get the course table, then check the number of rows in it
	//if zero rows, user has no courses so we redirect back to
	//home
	var course_table = document.getElementsByTagName('tbody');
	if (course_table[0].getElementsByTagName('tr').length == 0) {
		//ares home relative to domain
		document.location.href = '/ares';
	}
}

window.addEventListener ?
window.addEventListener("load",tugCheckForCourses,false) :
window.attachEvent && window.attachEvent("onload",tugCheckForCourses);