/* global rfc3986EncodeURIComponent */
var do_ares_push_ug = true;
var do_ares_push_uw = true;		
var do_ares_push_wlu = true;					
		
//when clicking ANY of the tabs, preload the details tab
$('.EXLTabsRibbon').live("click", function(event){
	if ( 
		(($('#guelph_header').length > 0) && do_ares_push_ug) ||
		(($('#waterloo_header').length > 0) && do_ares_push_uw) ||		
		(($('.WLUHeaderLinksContainer').length > 0) && do_ares_push_wlu)		
	) {
		tugProcessDetailTabLoad($(this));
	}			
});
		
function tugProcessDetailTabLoad(obj) {		
	var row_obj = $(obj).closest('.EXLSummary');	
	
	//create a new click event to pass to Primo function for loading details tab
	var event = new Event('click');		
	var detail_link = $(row_obj).find('.EXLDetailsTab a');	    
	tugselectAndLoadTab(event, detail_link, 'detailsTab');
}
			
$(".EXLTabHeaderButtonSendTo").live("click", function(event){
	//the tabs ribbon has been clicked, so lets process 
	//the ares push build if the current institution view wants it (the boolean)
	if ( 
		(($('#guelph_header').length > 0) && do_ares_push_ug) ||
		(($('#waterloo_header').length > 0) && do_ares_push_uw) ||		
		(($('.WLUHeaderLinksContainer').length > 0) && do_ares_push_wlu)		
	) {
		tugProcessAresPush($(this));
	}	
});

function tugProcessAresPush(obj) {
	//remove existing ares push to links so we don't get duplicates
	$(obj).find('.TUGAresPushToLink').remove();
	
	//determine the ares installation
	var dns = tugGetAresPushToDNS();
	
	//if we have a dns, continue to insert push link, else, skip
	if (dns.length > 0) {
		
		var link_list = tugGetAresPushToResourceForm(obj, dns);
		
		//insert the links
		for (var i in link_list) {	
			var push_link = link_list[i];
			$(obj).children('ol').append(push_link);
		}
	}		
}

//needed to escape apostrophe character and enfore encodes on others
//If you wish to use an encoding compatible with RFC 3986 (which reserves !, ', (, ), and *), you can use:
function rfc3986EncodeURIComponent (str) {
	//walk through and replace double straight quotes with smart curly quotes
	//first found is left, second is right
	var target = '"';
	var pos = str.indexOf(target);
	var left_curl = '“';
	var right_curl = '”';
	var loc_test = 1;
	while (pos > 0) {
		if (loc_test % 2) {			
			str = str.replace(target, left_curl);				
		}
		// even modulus, ie second occurance
		else {				
			str = str.replace(target, right_curl);			
		}	
		loc_test++;	
		pos = str.indexOf(target);
	}	
    return encodeURIComponent(str).replace(/[!'()*]/g, escape);  
}

//this is a modified primo function that will load a tab
//behind the scenes. It calls a current primo function
//so care must be taken during upgrades
function tugselectAndLoadTab(event, element, tabType)
{	
	var row_obj = $(element).closest('.EXLSummary');	//walk up to row	
	
    if ($(".gatherpagestat").val() == 'true') {
        if ($(element).parents('.EXLResultTab').size() > 0) {
            var elementStat = $(element).parents('.EXLResultTab').children('.EXLTabBoomId');
            if (elementStat != 'undefined' && elementStat != null && elementStat.length > 0) {
                var id = $(elementStat).get(0).id;
                boomCallToRum(id, false);
            }
        }
    }
    var tabsContainer = $(element).parents('.EXLSummary').find('.EXLContainer-' + tabType).get(0);
    if (tabsContainer && $(element).attr('target') != '_blank') {
        event.preventDefault();
        if (tabsContainer.tabUtils.state.status < exlTabState.FETCHED) {
            tabsContainer.tabUtils.clearTab();
        }
		tabsContainer.tabUtils.loadTab($(element).attr('href'));						
        if (tabType != 'recommendTab' && $(element).parents('.EXLSummary').find('.EXLContainer-recommendTab').get(0).tabUtils.state.status == exlTabState.UNFETCHED) {
            reportBibTip($(element).parents('.EXLResult').find('.EXLResultRecordId').attr('id'));
        }
    }										
}

// determine what the form will be for resource type & institution
function tugGetAresPushToResourceForm(obj, dns) {
	//get the classes of the result row...this will determine the resource type
	//var row_obj = $(obj).parent().parent().parent().parent().parent().parent();
	var link_list = [];	
	var row_obj = $(obj).closest('.EXLSummary');	
	var type_list = $(row_obj).parent().attr("class").toString().split(' ');

	//check for fail states / no detail data (although should be preloaded after ribbon click)
	var tmp_html = $(row_obj).find('.EXLContainer-detailsTab .EXLTabHeaderButtonSendTo').html();
	if (typeof tmp_html != 'undefined') {			
		if (tmp_html.length == 0) {					
			return [];
		}
	}
	else {				
		return [];
	}	
	var online = $(row_obj).find('.EXLResultAvailability').text().trim().toLowerCase();				
	var req_form_parms = '';
	var opt_form_parms = '';
	
	//get the details tab object
	var details_content_obj = $(row_obj).find('.EXLDetailsContent');
	var title = $(details_content_obj).find('.EXLLinkedFieldTitle').text().trim();
	var publisher = $(details_content_obj).find('[id^="Publisher"]').find('.EXLDetailsDisplayVal').text().trim();
	var create_date = $(details_content_obj).find('[id^="Creation Date"]').find('.EXLDetailsDisplayVal').text().trim();			
	var author = $(row_obj).find('.EXLResultAuthor').text().trim();	
	var edition = $(details_content_obj).find('[id^="Edition"]').find('.EXLDetailsDisplayVal').text().trim();
	
	//only use the first ISBN
	var ident = $(details_content_obj).find('[id^="Identifier"]').find('.EXLDetailsDisplayVal').text().trim();	
	
	//first try splitting with ';', this occurs when there are multiple
	//otherwise we'll try whwn it's just one
	//ISBN:9783642047916;ISBN:9783642047909 9783642047916
	//ISBN:9783642047916
	var ident_list = ident.split(';');
	
	if (ident_list.length >= 2) {
		ident_list = ident_list[0].split(':');
		if (ident_list.length >= 2) {
			ident = ident_list[1];
		}
	}
	else {
		ident_list = ident.split(':');
		if (ident_list.length >= 2) {
			ident = ident_list[1];
		}	
	}
	
	var call_no = $(row_obj).find('.EXLAvailabilityCallNumber').text().trim();
	call_no = call_no.replace('(','');
	call_no = call_no.replace(')','');

	//loop through the resource types and format
	for (var i in type_list) {	
		var type = type_list[i];
		
		if (type == 'EXLResultMediaTYPEbook') {
			var book_chap_parms = '';
			var e_book = '';
			var ares_title = 'ARES';
							
			opt_form_parms = 'Title='+ rfc3986EncodeURIComponent(title);
			opt_form_parms = opt_form_parms + '&Publisher=' + rfc3986EncodeURIComponent(publisher);						
			opt_form_parms = opt_form_parms + '&ISXN=' + rfc3986EncodeURIComponent(ident);
			opt_form_parms = opt_form_parms + '&CallNumber=' + rfc3986EncodeURIComponent(call_no);	
			opt_form_parms = opt_form_parms + '&PubDate=' + rfc3986EncodeURIComponent(create_date);	
			opt_form_parms = opt_form_parms + '&Edition=' + rfc3986EncodeURIComponent(edition);			
			opt_form_parms = opt_form_parms + '&Issue=' + rfc3986EncodeURIComponent(edition);			
												
			// for each institution, form url will differ
			if ($('#guelph_header').length > 0) {
				ares_title = 'ARES';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLBook';
				book_chap_parms = 'Action=10&Form=2&Value=IRFOpenURLBookChapter';
				e_book_parms = 'Action=10&Form=2&Value=IRFOpenURLEBook';
			}
			else if ($('#waterloo_header').length > 0) {
				ares_title = 'Reserves';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLBook';			
				book_chap_parms = 'Action=10&Form=2&Value=IRFOpenURLChapterElectronic';
				e_book_parms = 'Action=10&Form=2&Value=IRFOpenURLEBook';
			}
			else if ($('.WLUHeaderLinksContainer').length > 0) {
				ares_title = 'Ares';
				req_form_parms = 'Action=10&Form=2&Value=IRFBook';
				book_chap_parms = 'Action=10&Form=2&Value=IRFBookChapter';
				e_book_parms = 'Action=10&Form=2&Value=IRFEBook';
			}	
			
			if (opt_form_parms.length > 0) {
				// no author for book chapter
				book_chap_parms = book_chap_parms + '&' + opt_form_parms;							
				req_form_parms = req_form_parms + '&' + opt_form_parms + '&Author=' + rfc3986EncodeURIComponent(author);
				e_book_parms = e_book_parms + '&' + opt_form_parms + '&Author=' + rfc3986EncodeURIComponent(author);
								
				var push_link = '';
				
				if (online == 'available online') {
					push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
							<a href='" + dns + e_book_parms + "' title='" + ares_title + " - e-book' target='blank'>\
								<span class='EXLButtonSendToLabel'>" + ares_title + " - e-book</span>\
								<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
							</a>\
						</li>";												
					link_list.push(push_link);
					
					push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
							<a href='" + dns + book_chap_parms + "' title='" + ares_title + " - Book Chapter' target='blank'>\
								<span class='EXLButtonSendToLabel'>" + ares_title + " - Book Chapter</span>\
								<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
							</a>\
						</li>";												
					link_list.push(push_link);																			
				}				
				else {
					//if available online, as well as print
					var online_test = /available online/;
					if (online.search(online_test) > -1) {
						push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
								<a href='" + dns + e_book_parms + "' title='" + ares_title + " - e-book' target='blank'>\
									<span class='EXLButtonSendToLabel'>" + ares_title + " - e-book</span>\
									<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
								</a>\
							</li>";												
						link_list.push(push_link);									
					}
					
					push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
							<a href='" + dns + book_chap_parms + "' title='" + ares_title + " - Book Chapter' target='blank'>\
								<span class='EXLButtonSendToLabel'>" + ares_title + " - Book Chapter</span>\
								<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
							</a>\
						</li>";												
					link_list.push(push_link);			
								
					push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
							<a href='" + dns + req_form_parms + "' title='" + ares_title + " - Book' target='blank'>\
								<span class='EXLButtonSendToLabel'>" + ares_title + " - Book</span>\
								<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
							</a>\
						</li>";												
					link_list.push(push_link);													
				}
			}
		}		
		else if ((type == 'EXLResultMediaTYPEarticle') || (type == 'EXLResultMediaTYPEnewspaper_article') || (type == 'EXLResultMediaTYPEdissertation')) {			
			//basically a match of Journal, we'll keep separate incase some info
			//comes from somewhere else
			
			//article title comes from 'is part of'
			var part_of = $(details_content_obj).find('[id^="Is Part Of"]').find('.EXLDetailsDisplayVal').text().trim();
			var part_of_list = [];

			//didn't find the structured version of *Part Of*
			if (part_of.length == 0) {
				var part_of_unstruct = $(details_content_obj).find('[id^="Is Part Of"]').text().trim();
				var strip_part = part_of_unstruct.split(':');
				if (strip_part.length > 0){
					part_of = strip_part[1].trim();
				}
				else {
					part_of = "";
				}				
			}			
						
			part_of_list = part_of.split(',');
			var journal_title = '';
			
			if (part_of_list[0]) {
				journal_title = part_of_list[0];
			}
			
			//must iterate through rest of article description and check / parse out volume info -> it's not consistent
			var month_list = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec'];
			//month_list.push('january', 'february', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

			var vol = '';
			var vol_year = '';
			var vol_month = '';
			var page_ref = '';
												
			for (i = 1; i < part_of_list.length; i++) {
				var val_org = part_of_list[i];				
				var val = part_of_list[i].toLowerCase();				
				val = val.trim();
				val_org = val_org.trim();
				var vol_test = /^vol/;
				var year_test = /^[0-9]{4}/;		//will eventually break after 10000 -> I doubt primo / ares around :P
				var p1_test = /^p./;				
				var p2_test = /^pp./;				
				//is it the volume?						
				if (val.search(vol_test) > -1) {
					vol = val_org;					
				}
				//is it 4 digits
				else if (val.search(year_test) > -1) {
					//sometimes the month follows the year
					//quick and dirty test non reg ex
					var month_test = val.split(' ');
					var month_test_org = val_org.split(' ');					
					if (month_test.length > 1) {
						vol_year = month_test[0];
						for (n = 1; n < month_test.length; n++) {
							vol_month = vol_month + ' ' + month_test_org[n];	
						}						
						vol_month = vol_month.trim();
					}
					else {
						//just year
						vol_year = val_org;						
					}										
				}
				//is the a page reference
				else if ((val.search(p1_test) > -1) || (val.search(p2_test) > -1)) {
					var page_list = val_org.split('[');
					page_ref = page_list[0];

					//just remove p. and pp.					 
					page_ref = page_ref.replace(/pp./gi, "").trim();
					page_ref = page_ref.replace(/p./gi, "").trim();					
				}	
				//test for month		
				else {
					//check if a hit, otherwise, split and check for date -> need to check cases of , 7 1987 and find year
					var found_month = false;
					for (j = 0; j < month_list.length; j++) {						
						var month_test = new RegExp(month_list[j]);
						if (val.search(month_test) > -1) {
							found_month = true;
							//we found the month, also see if year is here (it sometimes the case)
							var year_test_2 = /[0-9]{4}$/;
							if (val.search(year_test_2) > -1) {
								var date_list = val.split(' ');
								var date_list_org = val_org.split(' ');
								for (k = 0; k < date_list.length; k++) {
									var val2 = date_list[k];
									var val2_org = date_list_org[k];									
									if (val2.search(month_test) > -1) {
										vol_month = val2_org;										
									}
									else if (val2.search(year_test) > -1) {
										vol_year = val2_org;
									} 
								}							
							}
							else {
								vol_month = val_org;								
							}																					
						}
					}
					if (!found_month) {
						var year_test_2 = /[0-9]{4}$/;
						if (val.search(year_test_2) > -1) {
							var date_list = val.split(' ');
							var date_list_org = val_org.split(' ');
							for (k = 0; k < date_list.length; k++) {
								var val2 = date_list[k];
								var val2_org = date_list_org[k];									
								if (val2.search(year_test) > -1) {
									vol_year = val2_org;
								} 
							}							
						}						
					}
				}
			}										
			
			//get an issue number, strip parenthesis from volume, and use value in it as issue# 		
			var issue = '';
			var p1 = vol.indexOf("(");
			if (p1 > 0) {
				//we have parenthesis, strip out of original string
				var p2 = vol.indexOf(")");
				if (p2 > 0 && p2 > p1) {
					//we're good to strip
					issue = vol.slice( p1+1,p2);
					if (vol.length > p2) {
						//something after the (), append
						vol = vol.slice(0,p1) + ' ' + vol.slice(p2+1);
					}
					else {
						vol = vol.slice(0,p1);						
					}										
				}				
			}
			
			//get the DOI if there is one
			var doi_ident = $(details_content_obj).find('[id^="Identifier"]').find('.EXLDetailsDisplayVal').text().trim();
			var doi_dns= 'https://doi.org/';			
			var doi = '';						
			var p1 = doi_ident.indexOf("DOI:");
			if (p1 > 0) {
				var tmp = doi_ident.slice( p1+4);
				var doi_list = tmp.split(';');
				if (doi_list.length > 0) {
					doi = doi_dns + doi_list[0].trim();
				}		
			}

			//strip the vol. value from the actual volume being pushed
			vol = vol.replace(/vol./gi, "").trim();			
			vol = vol.replace(/vol/gi, "").trim();
	
			opt_form_parms = 'Title='+ rfc3986EncodeURIComponent(journal_title);			
			opt_form_parms = opt_form_parms + '&ArticleTitle=' + rfc3986EncodeURIComponent(title);						
			opt_form_parms = opt_form_parms + '&Author=' + rfc3986EncodeURIComponent(author);			
			opt_form_parms = opt_form_parms + '&ISXN=' + rfc3986EncodeURIComponent(ident.trim());	
			opt_form_parms = opt_form_parms + '&Volume=' + rfc3986EncodeURIComponent(vol);
			opt_form_parms = opt_form_parms + '&Issue=' + rfc3986EncodeURIComponent(issue);
			opt_form_parms = opt_form_parms + '&DOI=' + rfc3986EncodeURIComponent(doi);			
						
			if (type == 'EXLResultMediaTYPEdissertation') {
				opt_form_parms = opt_form_parms + '&JournalYear=' + rfc3986EncodeURIComponent(create_date);				
			}
			else {
				opt_form_parms = opt_form_parms + '&JournalYear=' + rfc3986EncodeURIComponent(vol_year);				
			}	
			opt_form_parms = opt_form_parms + '&JournalMonth=' + rfc3986EncodeURIComponent(vol_month);
			opt_form_parms = opt_form_parms + '&Pages=' + rfc3986EncodeURIComponent(page_ref);						
																
			// for each institution, form url will differ
			if ($('#guelph_header').length > 0) {
				ares_title = 'ARES';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLArticle';
			}
			else if ($('#waterloo_header').length > 0) {
				ares_title = 'Reserves';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLArticleElectronic';				
			}
			else if ($('.WLUHeaderLinksContainer').length > 0) {
				ares_title = 'Ares';				
				req_form_parms = 'Action=10&Form=21&Value=Course';
			}	
			
			if (opt_form_parms.length > 0) {			
				req_form_parms = req_form_parms + '&' + opt_form_parms;
												
				var push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
						<a href='" + dns + req_form_parms + "' title='" + ares_title + " - Article' target='blank'>\
							<span class='EXLButtonSendToLabel'>" + ares_title + " - Article</span>\
							<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
						</a>\
					</li>";												
				link_list.push(push_link);													
			}																					
		}		
		else if (type == 'EXLResultMediaTYPEvideo') {
			opt_form_parms = 'Title='+ rfc3986EncodeURIComponent(title);
			opt_form_parms = opt_form_parms + '&Author=' + rfc3986EncodeURIComponent(author);			
			opt_form_parms = opt_form_parms + '&Publisher=' + rfc3986EncodeURIComponent(publisher);
			opt_form_parms = opt_form_parms + '&CallNumber=' + rfc3986EncodeURIComponent(call_no);
			opt_form_parms = opt_form_parms + '&PubDate=' + rfc3986EncodeURIComponent(create_date);
			opt_form_parms = opt_form_parms + '&Edition=' + rfc3986EncodeURIComponent(edition);						
									
			// for each institution, form url will differ
			if ($('#guelph_header').length > 0) {
				ares_title = 'ARES';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLVideo';
			}
			else if ($('#waterloo_header').length > 0) {
				ares_title = 'Reserves';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLVideo';
			}
			else if ($('.WLUHeaderLinksContainer').length > 0) {
				ares_title = 'Ares';				
				req_form_parms = 'Action=10&Form=2&Value=IRFAudio';			
			}	
			
			if (opt_form_parms.length > 0) {
			
				req_form_parms = req_form_parms + '&' + opt_form_parms;
												
				var push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
						<a href='" + dns + req_form_parms + "' title='" + ares_title + " - Article' target='blank'>\
							<span class='EXLButtonSendToLabel'>" + ares_title + " - Video</span>\
							<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
						</a>\
					</li>";
				link_list.push(push_link);
			}			
		}							
		else if (type == 'EXLResultMediaTYPEaudio') {
			opt_form_parms = 'Title='+ rfc3986EncodeURIComponent(title);
			opt_form_parms = opt_form_parms + '&Author=' + rfc3986EncodeURIComponent(author);			
			opt_form_parms = opt_form_parms + '&Publisher=' + rfc3986EncodeURIComponent(publisher);
			opt_form_parms = opt_form_parms + '&CallNumber=' + rfc3986EncodeURIComponent(call_no);
			opt_form_parms = opt_form_parms + '&PubDate=' + rfc3986EncodeURIComponent(create_date);
			opt_form_parms = opt_form_parms + '&Edition=' + rfc3986EncodeURIComponent(edition);								
			
			// for each institution, form url will differ
			if ($('#guelph_header').length > 0) {
				ares_title = 'ARES';
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLAudio';
			}
			else if ($('#waterloo_header').length > 0) {
				ares_title = 'Reserves';				
				req_form_parms = 'Action=10&Form=2&Value=IRFOpenURLAudio';
			}
			else if ($('.WLUHeaderLinksContainer').length > 0) {
				ares_title = 'Ares';				
				req_form_parms = 'Action=10&Form=2&Value=IRFAudio';			
			}	
			
			if (opt_form_parms.length > 0) {
			
				req_form_parms = req_form_parms + '&' + opt_form_parms;
												
				var push_link = "<li class='EXLButtonSendToDelicious TUGAresPushToLink'>\
						<a href='" + dns + req_form_parms + "' title='" + ares_title + " - Article' target='blank'>\
							<span class='EXLButtonSendToLabel'>" + ares_title + " - Audio</span>\
							<span class='EXLButtonSendToIcon EXLButtonSendToIconEndNote'></span>\
						</a>\
					</li>";												
				link_list.push(push_link);													
			}					
		}									
	}	//end of type loop
			
	return link_list;
}

// the tugGetAresPushToDNS function determines what the ares dns will be, based
// on institution and what primo server we're running on (staging) and if the
// institution wants to use an ares dev server
function tugGetAresPushToDNS() {
	var ug_force_prod = true;
	var wlu_force_prod = true;

	var dns = '';
	var url = window.location.href;
	
	var is_prod = false;
	
	// are we running on primo staging?
	if (url.indexOf('tugprimostage.lib.uwaterloo.ca') > -1) {	
		is_prod = false;
	}
	
	if ($('#guelph_header').length > 0) {
		if (is_prod || ug_force_prod) {
			dns = 'https://ares.lib.uoguelph.ca/ares/ares.dll?';
		}
		else {
			dns = 'http://aresdev.lib.uoguelph.ca/ares/ares.dll?';
		}
	}
	else if ($('#waterloo_header').length > 0) {
		dns = 'https://www.reserves.uwaterloo.ca/ares/ares.dll?';
	}
	else if ($('.WLUHeaderLinksContainer').length > 0) {
		if (is_prod || wlu_force_prod) {
			dns = 'https://ares.wlu.ca/ares/ares.dll?';
		}
		else {
			dns = 'https://libares2.wlu.ca/ares/ares.dll?';
		}	
	}	
	return dns;
}